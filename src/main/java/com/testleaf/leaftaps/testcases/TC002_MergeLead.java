package com.testleaf.leaftaps.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.testng.api.base.Annotations;
import com.testleaf.leaftaps.pages.LoginPage;
import com.testleaf.leaftaps.pages.LoginPageMerge;

public class TC002_MergeLead extends Annotations{
	@BeforeTest
	public void setdata()
	{
		testcaseName="TC002_MergeLead";
		testcaseDec="Merge Lead";
		author="Divya";
		category="Happy FLow-MergeLead";
	}
	@Test
public void mergeLead()
{
	new LoginPageMerge().enterUserName().enterPwd().clickLogin()
	.clickCrmsfa().clickLead().clickMergeLead().clickFromLeadLink().clickFirstResultLead()
	.clickToLeadLink().clickSecondResultLead().clickMerge()
	.clickAcceptalert().verifyTitle();
	}
}
