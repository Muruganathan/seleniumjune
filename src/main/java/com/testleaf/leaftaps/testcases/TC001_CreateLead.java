package com.testleaf.leaftaps.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.testng.api.base.Annotations;
import com.testleaf.leaftaps.pages.LoginPage;

public class TC001_CreateLead extends Annotations {
@BeforeTest
	public void setdata()
	{
	testcaseName="TC001_CreateLead";
	testcaseDec="Creating Lead";
	author="Divya";
	category="Happy FLow-CreateLead";
	excelFileName="TC001_CreateLead";
	}
	
	@Test(dataProvider="fetchData")
	public void createLead(String compname,String firstname,String lastname)
	{
		new LoginPage().enterUserName().enterPwd().clickLogin()
		.clickCrmsfa().clickLead().clickCreateLead()
		.enterCompanyName(compname).enterFirstName(firstname).enterLastName(lastname)
		.clickCreate().verifyTitle();
	}
}
