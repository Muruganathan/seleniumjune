package com.testleaf.leaftaps.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MergeLeadPage extends Annotations {
public MergeLeadPage() {}

public MergeLeadPage clickFromLeadLink()
{
		click(locateElement("xpath", "//img[@alt='Lookup']"));
	return this;
}
public MergeLeadPage clickFirstResultLead()
{
	switchToWindow(1);
	click(locateElement("xpath", "(//a[@class='linktext'])[1]"));
	switchToWindow(0);
	return this;
}
public MergeLeadPage clickToLeadLink()
{
		click(locateElement("xpath", "(//img[@alt='Lookup'])[2]"));
	return this;
}

public MergeLeadPage clickSecondResultLead()
{
	switchToWindow(1);
	click(locateElement("xpath", "(//a[@class='linktext'])[7]"));
	switchToWindow(0);
	return this;
}

public MergeLeadPage clickMerge()
{
	click(locateElement("xpath", "//a[text()='Merge']"));
	return this;
}

public ViewLeadPage clickAcceptalert()
{
	acceptAlert();
	return new ViewLeadPage();
}



}
