package com.testleaf.leaftaps.pages;

import com.autoBot.testng.api.base.Annotations;

public class LeadPage extends Annotations {
	
LeadPage(){}
public CreateLeadPage clickCreateLead()
{
	click(locateElement("id", "createLeadForm_companyName"));
	return new CreateLeadPage();
}

}
