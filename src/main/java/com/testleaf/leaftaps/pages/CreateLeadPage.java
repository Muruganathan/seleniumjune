package com.testleaf.leaftaps.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations {
	
CreateLeadPage(){
	PageFactory.initElements(driver, this);
}
@FindBy(how=How.ID,using="createLeadForm_firstName")
private WebElement eleFN;

@FindBy(how=How.ID,using="createLeadForm_lastName")
private WebElement eleLN;

@FindBy(how=How.ID,using="createLeadForm_companyName")
private WebElement eleCN;

public CreateLeadPage enterFirstName(String firstname)
{
	clearAndType(eleFN, firstname);
	return this;
}
public CreateLeadPage enterLastName(String lastname)
{
	clearAndType(eleLN, lastname);
	return this;
}
public CreateLeadPage enterCompanyName(String compname)
{
	clearAndType(eleCN, compname);
	return this;
}
public ViewLeadPage clickCreate()
{
	click(locateElement("name", "submitButton"));
	return new ViewLeadPage();
}


}
