package com.testleaf.leaftaps.pages;

import com.autoBot.testng.api.base.Annotations;

public class HomePage extends Annotations {
	
HomePage(){}

public MyHomePage clickCrmsfa()
{
	click(locateElement("link", "CRM/SFA"));
	return new MyHomePage();
}
}
