package com.testleaf.leaftaps.pages;

import com.autoBot.testng.api.base.Annotations;

public class NavigateMergelead extends Annotations{
public NavigateMergelead() {}

public MergeLeadPage clickMergeLead()
{
	click(locateElement("link", "Merge Leads"));
	return new MergeLeadPage();
}
}
