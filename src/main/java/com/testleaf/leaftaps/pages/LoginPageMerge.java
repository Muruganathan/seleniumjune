package com.testleaf.leaftaps.pages;

import com.autoBot.testng.api.base.Annotations;

public class LoginPageMerge extends Annotations {
	
public LoginPageMerge(){}

public LoginPageMerge enterUserName()
{
	clearAndType(locateElement("id", "username"),"DemoSalesManager");
	return this;
}
public LoginPageMerge enterPwd()
{
	clearAndType(locateElement("id", "password"),"crmsfa");
	return this;
}
public  HomePageMerge clickLogin()
{
	click(locateElement("class", "decorativeSubmit"));
	return new HomePageMerge();
}
}
