package com.testleaf.leaftaps.pages;

import com.autoBot.testng.api.base.Annotations;

public class LoginPage extends Annotations {
	
public LoginPage(){}

public LoginPage enterUserName()
{
	clearAndType(locateElement("id", "username"),"DemoSalesManager");
	return this;
}
public LoginPage enterPwd()
{
	clearAndType(locateElement("id", "password"),"crmsfa");
	return this;
}
public  HomePage clickLogin()
{
	click(locateElement("class", "decorativeSubmit"));
	return new HomePage();
}
}
